let wrapper = document.querySelector('.wrapper');

let requestURL = './src/json/products.json';
let request = new XMLHttpRequest();
request.open('GET', requestURL);
request.responseType = 'json';
request.send();
request.onload = function() {
    let products = request.response;
    showProducts(products);
};

function showProducts(jsonObj) {
    let products = jsonObj['products'];

    for (let i = 0; i < products.length; i++) {
        let productCardWrapper = document.createElement('div');
        productCardWrapper.className = 'product__card-wrapper';
        let productCard = document.createElement('div');
        productCard.className = 'product__card';
        let productCardImgTop = document.createElement('img');
        productCardImgTop.className = 'product__card-img-top';
        let productCardBody = document.createElement('div');
        productCardBody.className = 'product__card-body';
        let productCardTitle = document.createElement('h5');
        productCardTitle.className = 'product__card-title';
        let productCardText = document.createElement('a');
        productCardText.className = 'product__card-text';

        productCardText.textContent = products[i].text;
        let imgItem = document.createAttribute('src');
        imgItem.value = products[i].img;
        productCardImgTop.setAttributeNode(imgItem);

        let iidItem = document.createAttribute('id');
        iidItem.value = products[i].id;
        productCardWrapper.setAttributeNode(iidItem);

        productCardWrapper.appendChild(productCard);
        productCard.appendChild(productCardImgTop);
        productCard.appendChild(productCardBody);
        productCardBody.appendChild(productCardTitle);
        productCardTitle.appendChild(productCardText);

        wrapper.appendChild(productCardWrapper);
    }
}

function filterProducts(val) {

    let productCardWrapper = document.getElementsByClassName('product__card-wrapper');
    window.history.pushState('',"", '?id=' + val);

    for (let i = 0; i < productCardWrapper.length; i++) {
        if (val === productCardWrapper[i].id ) {
            productCardWrapper[i].style.display = 'flex';
        } else {
            productCardWrapper[i].style.display = 'none';
        }
    }
}

function filterProductsAll(val) {

    let productCardWrapper = document.getElementsByClassName('product__card-wrapper');
    window.history.pushState('',"", '?id=' + val);

    for (let i = 0; i < productCardWrapper.length; i++) {
        if (val === '0') {
            productCardWrapper[i].style.display = 'flex';
        }
    }
}

function searchProducts(val) {

    let productCardWrapper = document.getElementsByClassName('product__card-wrapper');

    for (let i = 0; i < productCardWrapper.length; i++) {
        if (val === productCardWrapper[i].textContent) {
            productCardWrapper[i].style.display = 'flex';
        } else {
            productCardWrapper[i].style.display = 'none';
        }
    }
}


function onFocus(val) {
    let icon = document.getElementById('icon');
    if (val === 'onFocus') {
        icon.style.backgroundPosition  = '0 -24px';
    } else if (val === 'offFocus') {
        icon.style.backgroundPosition  = '0 0';
    }
}

