const path = require('path');
const webpack = require('webpack');
const HtmlPlugin = require('html-webpack-plugin');

const PATHS = {
    src: path.join(__dirname, 'src'),
    img: path.join(__dirname, 'src/img'),
    styles: path.join(__dirname, 'src/css'),
    build: path.join(__dirname, 'build')
}

module.export = {

    context: path.resolve(__dirname, 'src'),
    entry: {
        script: ['./scripts/main.js', './styles/main.scss'],
        index: './index.html'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "bundle.js"
    },

    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    },
    plugins: [
        new HtmlPlugin({
            title: 'Webpack dev server'
        }),
        new webpack.HotModuleReplacementPlugin()
    ],
    devServer: {
        hot: true
    }

}