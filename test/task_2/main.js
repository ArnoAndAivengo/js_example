const departure = ''; // точка отправления в рамках отрезка маршрута
const destination = ''; // точка назначения в рамках отрезка маршрута
const seat = ''; // номер места в транспорте. может быть не указан (No seat assignment)
const transport_type = ''; // тип транспорта (bus, train, flight, ferry, etc.)
const departure_point = ''; // дополнительная информация о точке отправления из билетов. Разная для разных типов транспорта (gate, platform, pier, etc.)
const transport_number = ''; // номер транспорта (хз)
const baggage = ''; // информация, касающаяся багажа (если она указана)


function sorter() {
    const city = [
        {
            departure: "Madrid",
            destination: "Barcelona",
            seat: '45B',
            transport_type: 'train',
            departure_point: '',
            transport_number: '78A',
            baggage: ''
        },
        {
            departure: "Barcelona",
            destination: "Gerona Airport",
            seat: '',
            transport_type: 'airport bus',
            departure_point: 'No seat assignment',
            transport_number: '',
            baggage: ''
        },
        {
            departure: "Gerona Airport",
            destination: "Stockholm",
            seat: 'Gate 45B. Seat 3A.',
            transport_type: 'flight',
            departure_point: '',
            transport_number: 'SK455',
            baggage: 'Baggage drop at ticket counter 344'
        },
        {
            departure: "Stockholm",
            destination: "New York JFK.",
            seat: 'Gate 22 Seat 7B.',
            transport_type: 'flight',
            departure_point: '',
            transport_number: 'GSK22',
            baggage: 'Baggage will be automatically transferred from your last leg.'
        }
    ]

   for (let i = 0; i < city.length; i++) {

       const destination = city[i].destination;
       const departure = city[i].departure;

   
       
   }
   console.log(destination + '  ' + departure)
}
sorter()
