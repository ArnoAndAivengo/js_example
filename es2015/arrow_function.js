const aw = (x) => x*x;

console.log(aw(3));

const arr = ['1', '3', '2', '6']

const res = arr
    .map((el) => parseInt(el)) // преобразуем в числовой формат
    .filter((num) => num%2) // находим нечетное число
    .reduce((max, value) => Math.max(max, value), 0); // находим максимум

console.log(res)


export {
    arr, res
};