const car = {
    name: `lexus`,
    color: `black`,
    speed: `450`,
    year: `2019`
};

const {name: carName, color: carColor, year: carYear} = car;

console.log(`Марка: ${carName}, цвет: ${carColor}, год: ${carYear}`);


const x = 10;
const y = 30;
const point = {
    x: x,
    y: y,
    draw: function(){
        //...
    }
};

const p = {
    x, 
    y,
    draw(ctx) {
        
    }
}


const prefix = '_blah_';

const data = {
    [prefix + 'name']: 'Bob',
    [prefix + 'age']: 23
}

console.log(data);

//----------//

const defaults = {
    host: 'localhost',
    dbName: 'blog',
    user: 'admin'
};

const opts = {
    user: 'john',
    password: 'utopia'
};

const res = Object.assign({}, defaults, opts);

console.log(res);