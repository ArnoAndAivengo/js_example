const array = [1, 2, 3, 'Hello']
console.log(array)

// const first = array[0];
// const third = array[2];

//obj
const [third, first, hello] = array;
console.log(first, third, hello)


const person = {
    name: 'Alex',
    year: 33,
    sex : 'man'
}

const {name: name, sex: sex} = person;

console.log(name, sex)

function connect({
    host = 'locahost',
    port = 8080,
    user = 'user'} = {}) {
         console.log('user:', user, 'port:', port, 'host:', host)
}

connect({port: 8585});


// array
const fib = [1, 1, 2, 3, 5, 8, 13];

const line = [[10, 17], [14, 7]];

const [[p1x, p1y], [p2x, p2y]] = line;


const dict = {
    duck: 'quack',
    dog: 'wuff',
    mouse: 'squeak',
    hamster: 'squeak'
};

const res = Object.entries(dict)
    .filter(([,value]) => value === 'squeak')
    .map(([key]) => key);

console.log(res)