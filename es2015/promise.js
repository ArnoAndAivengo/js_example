function oldDelay(ms, func) {
    setTimeout(function() {
        func();
    },ms)
}

// oldDelay(3000, function () {
//     console.log('Old delay passed!')
// });

function delay(ms = 2000) {
    return new Promise( (resolve, reject) => {
        setTimeout(() => {
            reject();
        }, ms)
    });
}

delay(1000)
    .then(() => {
        console.log('Provoz vezet drove!');
    })
    .catch(() => {
        console.log('error');
})

//http://date.jsontest.com


