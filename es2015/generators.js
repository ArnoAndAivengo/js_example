function* getRange(start = 0, end = 100, step = 10) {
    while ( start < end ) {
        yield start;
        start += step;
    }
}

for ( let n of getRange( 10, 50, 10 )) {
    console.log(n)
}


const fib = {
   * [Symbol.iterator]() {
        let pre = 0, cur = 1;
        for (;;) {
            [pre, cur] = [cur, pre + cur];
            yield cur;
        }
    }
};

for (let n of fib) {
    if (n > 1500) break;
    console.log(n)
}