
// assign
let obj1 = {a: 1};
let obj2 = {b: 2, c: 3};

let obj3 = Object.assign({}, obj1, obj2);

console.log('Obj3', obj3);


let findeditem = [1, 2, 3, 4].find(x => x > 3);
console.log(findeditem);

let str = "Pakasaixa";

console.log('Repeat: ', str.repeat(3));
console.log('StartsWith: ', str.startsWith('el', 1));
console.log('Includes: ', str.includes('llo', 2));