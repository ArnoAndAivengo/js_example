class Animal {
    constructor(name, voice) {
        this.name = name;
        this.voice = voice;
    }

    say() {
        console.log(this.name, 'goes', this.voice);
    }
}

class bird extends Animal {
    constructor( name, voice, canFly) {
        super(name, voice);
        super.say();
        this.canFly = canFly;
    }

    say() {
        console.log('Birds don\'t like to talk');
    }
}

const duck = new bird('Duck', 'quack', true);

duck.say();