Chart.pluginService.register({
    afterUpdate: function (chart) {
        if (chart.config.options.elements.center) {
            var helpers = Chart.helpers;
            var centerConfig = chart.config.options.elements.center;
            var globalConfig = Chart.defaults.global;
            var ctx = chart.chart.ctx;

            var fontStyle = helpers.getValueOrDefault(centerConfig.fontStyle, globalConfig.defaultFontStyle);
            var fontFamily = helpers.getValueOrDefault(centerConfig.fontFamily, globalConfig.defaultFontFamily);

            if (centerConfig.fontSize)
                var fontSize = centerConfig.fontSize;
            // figure out the best font size, if one is not specified
            else {
                ctx.save();
                var fontSize = helpers.getValueOrDefault(centerConfig.minFontSize, 1);
                var maxFontSize = helpers.getValueOrDefault(centerConfig.maxFontSize, 256);
                var maxText = helpers.getValueOrDefault(centerConfig.maxText, centerConfig.text);

                do {
                    ctx.font = helpers.fontString(fontSize, fontStyle, fontFamily);
                    var textWidth = ctx.measureText(maxText).width;

                    // check if it fits, is within configured limits and that we are not simply toggling back and forth
                    if (textWidth < chart.innerRadius * 2 && fontSize < maxFontSize)
                        fontSize += 1;
                    else {
                        // reverse last step
                        fontSize -= 1;
                        break;
                    }
                } while (true)
                ctx.restore();
            }

            // save properties
            chart.center = {
                font: helpers.fontString(fontSize, fontStyle, fontFamily),
                fillStyle: helpers.getValueOrDefault(centerConfig.fontColor, globalConfig.defaultFontColor)
            };
        }
    },
    afterDraw: function (chart) {
        if (chart.center) {
            var centerConfig = chart.config.options.elements.center;
            var ctx = chart.chart.ctx;

            ctx.save();
            ctx.font = chart.center.font;
            ctx.fillStyle = chart.center.fillStyle;
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            var centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
            var centerY = (chart.chartArea.top + chart.chartArea.bottom) / 2;
            ctx.fillText(centerConfig.text, centerX, centerY);
            ctx.restore();
        }
    },
});

function legends(int, evt) {
    var legends__text = document.getElementsByClassName("legends__text");
    var tablinks = document.getElementsByClassName("legends__diagram-blocks");
    var description = document.getElementsByClassName("description");
    var canvas__block = document.getElementsByClassName("canvas__block");
    var dis = [
        'Средний вес',
        'Пробег лок.',
        'Производ. лок',
        'Производ. лок',
    ];
    var center = [
        'ЭП',
        'ЭП',
        'ЭП',
        'РП',
    ];

    document.getElementById('legends').style.display = "block";
    if (evt) {
        for (var i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        evt.currentTarget.className += " active";
    }

    $.when(
        $.ajax({
            url: 'resources/legends_blocks.json',
            success: function(responseText) {
                var store = responseText.data[int];
                var arr = [];
                var data = responseText.data.time_create.split(' ');

                legends__text[0].innerHTML = '<p>Выполнение эксплуатационных покзателей ДВП на ' + data[1] + ' МСК ' + data[0] +' г.</p>';

                Object.keys(store).map(function (key) {
                    arr.push(key);
                });

                for (var i = 0; i < arr.length; i++) {
                    if (store[arr[i]].percent) {

                        let tooltiptext = document.createElement('span');
                        tooltiptext.className = 'tooltiptext';
                        tooltiptext.innerHTML = 'Факт: ' + store[arr[i]].fact + '<br>' + 'План: ' + store[arr[i]].plan;
                        canvas__block[i].appendChild(tooltiptext);
                        new Chart(document.getElementById("canvas__item_" + [i]).getContext("2d"), {
                            type: 'doughnut',
                            data: {
                                labels: ["Процент"],
                                datasets: [{
                                    backgroundColor: [
                                        "#FF6384",
                                        "#808080",
                                    ],
                                    borderColor: "#808080",
                                    data: [store[arr[i]].percent, store[arr[i]].percent - 100]
                                }]
                            },

                            options: {
                                hover: false,
                                elements: {
                                    center: {
                                        maxText: '100%',
                                        text: center[i],
                                        fontColor: '#000',
                                        fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
                                        fontStyle: 'normal',
                                        minFontSize: 1,
                                        maxFontSize: 256,
                                    }
                                },
                                legend: {
                                    display: false
                                },
                                tooltips: false,
                            }
                        });
                        description[i].innerHTML = '<p> ' + store[arr[i]].percent + '%' + '</p> <hr> <span style="color: #000000">' + dis[i] + '</span>';
                    }
                }
            }
        })
    )
}

legends('vp')