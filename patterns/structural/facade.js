class Complaints {
    constructor() {
        this.complaints = [];
    }
    reply(complaint) {}

    add(complaint) {
        this.complaints.push(complaint);
        return this.reply(complaint);
    }
}

class productComplaints extends Complaints {
    reply({id, customer, details}) {
        return `Product: ${id}: ${customer} (${details})`
    }
}

class serviceComplaints extends Complaints {
    reply({id, customer, details}) {
        return `Service: ${id}: ${customer} (${details})`
    }
}

class ComplaintRegistry {
    register(customer, type, details) {
        const id = Date.now();
        let complaints;

        if (type === 'service') {
            complaints = new serviceComplaints()
        } else {
            complaints = new productComplaints()
        }
        return complaints.add({id, customer, details})
    }
}

const registry = new ComplaintRegistry();

console.log(registry.register('Aleksandr', 'service', 'dont took'));