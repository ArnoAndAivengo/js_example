const car = {
  wheels: 4,
  init() {
      console.log(`My car have ${this.wheels} wheels, my car ${this.car}.`)
  }
};

const carWithOwner = Object.create(car, {
    car: {
        value: "Jeep"
    }
});

console.log(carWithOwner.__proto__ === car)

carWithOwner.init();