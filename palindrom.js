

function isPalindrom(word) {
    const length = word.length;
    const half = Math.floor(length / 2);

    for (let index = 0; index < half; index++) {
        if (word[index] !== word[length - index - 1]) {
            return false
        }
    }
    return true;
}

console.log(isPalindrom('madam'));
console.log(isPalindrom('hello'));