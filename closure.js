function Myobject(name, message) {
    this.name = name.toString();
    this.message = message.toString();
}

(function() {
    this.getName = function() {
        return this.name;
    };
    this.getMessage = function() {
        return this.message;
    };
}).call(Myobject.prototype)


let obj = new Myobject('string', 'string')
console.log(obj)