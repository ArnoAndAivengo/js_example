// anagram

const wordOne = "hello";
const wordTwo = "llohe";

const result1 = wordOne.split("").reverse().sort().join();
const result2 = wordTwo.split("").reverse().sort().join();

console.log(result1);
console.log(result2);
console.log(result1 === result2);
