function calc(n) {
    return function() {
    }
    console.log( 3 * n )
}

const calcs = calc(9);

calcs()

function urlGenerator(domain) {
    return function (url) {
        return `https://${url}.${domain}`
    }
}

const comUrl = urlGenerator('com');
const ruUrl = urlGenerator('ru');

console.log(comUrl('google'));
console.log(ruUrl('yandex'));


function bind(context, fn) {
    return function(...args) {
        fn.apply(context, args)
    }
}

function logPerson() {
    console.log(`Person: ${this.name}, ${this.age}, ${this.job}`)
}

const person1 = { name: 'Александр', age: 34, job: 'Frontend Developer'};
const person2 = { name: 'Наталья', age: 32, job: 'Designer'};

bind(person1, logPerson)();
bind(person2, logPerson)();