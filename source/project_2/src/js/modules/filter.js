const filter = () => {

  const menu = document.querySelector('.portfolio-menu'),
        items = document.querySelectorAll('li'),
        btnAll = document.querySelector('.all'),
        btnLovers = document.querySelector('.lovers'),
        btnChef = document.querySelector('.chef'),
        btnGirl = document.querySelector('.girl'),
        btnGuy = document.querySelector('.guy'),
        btnGrandmother = document.querySelector('.grandmother'),
        btnGranddad = document.querySelector('.granddad'),
        wrapper = document.querySelector('.portfolio-wrapper'),
        markAll = document.querySelectorAll('.all'),
        markLovers = document.querySelectorAll('.lovers'),
        markChef = document.querySelectorAll('.chef'),
        markGirl = document.querySelectorAll('.girl'),
        markGuy = document.querySelectorAll('.guy'),
        no = document.querySelector('.portfolio-no');

  const typeFilter = (markType) => {
    markAll.forEach(mark => {
      mark.style.display = 'none';
      mark.classList.remove('animated', 'fadeIn');
    });

    no.style.display = 'none';
    no.classList.remove('animated', 'fadeIn');

    if (markType) {
      markType.forEach(mark => {
        mark.style.display = 'block';
        mark.classList.add('animated', 'fadeIn');
      })
    } else {
      no.style.display = 'block';
      no.classList.add('animated', 'fadeIn');
    }
  };


  btnAll.addEventListener('click', () => {
    typeFilter((markAll));
  });

  btnLovers.addEventListener('click', () => {
    typeFilter(markLovers)
  });

  btnChef.addEventListener('click', () => {
    typeFilter(markChef)
  });

  btnGirl.addEventListener('click', () => {
    typeFilter(markGirl)
  });

  btnGuy.addEventListener('click', () => {
    typeFilter(markGuy)
  });

  btnGranddad.addEventListener('click', () => {
    typeFilter()
  });

  btnGrandmother.addEventListener('click', () => {
    typeFilter()
  });

  menu.addEventListener('click', (e) => {
    let target = e.target;

    if (target && target.tagName == "LI") {
      items.forEach(btn => btn.classList.remove('active'));
      target.classList.add('active');
    }
  })

};

export default filter;