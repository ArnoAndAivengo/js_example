var store = {
    nextId: 1,
    cache: {},
    add: function(fn) {
        if (!fn.id) {

            fn.id = this.nextId++;
            this.cache[fn.id] = fn;
            console.log(this)
            return true;
        }
    }
};

var obj = {},
    obj1 = {},
    obj2 = {};

// function nanga() {}
// function nangsa() {}
// function ssss() {}
console.assert(store.add(obj),
    "Function was safely added.");
console.assert(store.add(obj1),
    "But it was only added once");
console.assert(store.add(obj2),
    "But it was only added once");

