function isPrime (value) {

    if(!isPrime.answers) {
        isPrime.answers = {};
    }

    if (isPrime.answers[value] !== undefined) {
        return isPrime.answers[value];
    }

    var prime = value !== 0 && value !== 1; // 1 не простое число

    for (var i = 2; i < value; i++) {
        if (value % i == 0) {
            prime = false;
            break
        }
    }

    return isPrime.answers[value] = prime;
}

console.assert(isPrime(1), "5 is prime");
console.assert(isPrime(3), "5 is prime");
console.assert(isPrime(0), "5 is prime");
console.assert(isPrime(10), "5 is prime");
console.log(isPrime.answers)
console.assert(isPrime.answers[5], "The answer was cached!");