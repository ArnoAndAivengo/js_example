function Peoples(gender, age) {
    this.gender = gender || 'Оно';
    this.age = age || 12;
}

Peoples.prototype.name = function(){
    console.log('name people is: ', this.gender)
};

Peoples.prototype.age = function(){
    console.log('age people is: ', this.age)
}

module.exports = {
    Peoples: Peoples
}

// global.Peoples = Peoples;