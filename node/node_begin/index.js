// var Peoples = require('./Peoples.js');
// require('./Peoples.js');

// var managers = new Peoples.Peoples('Managers');
// var directors = new Peoples.Peoples('Director');
// managers.name();
// directors.name();

// var _ = require('lodash');
//
// console.log('Lodash sum: ' + _.sum([4,11]))


// var util = require('util');
//
//
// function Car() {}
//
// Car.prototype.logName = function(){
//     console.log('This name is:', this.name);
// };
//
// function BMW(name) {
//     this.name = name || 'Unknown BWM model'
// };
//
// BMW.prototype.drive = function() {
//     console.log('I\'m driving!')
// };
//
// util.inherits(BMW, Car);
//
// var bmw = new BMW('X7');
// bmw.logName();
// bmw.drive();


// var EventEmitter = require('events').EventEmitter;
//
// var dispatcher = new EventEmitter();
//
// dispatcher.on('connect', function(data){
//     console.log('Connect 1', data)
// });
//
// dispatcher.on('click', function(data){
//     console.log('Click 1', data)
// });
//
// dispatcher.on('error', function(err){
//     console.warn(err)
// });
//
// dispatcher.emit('connect', {foo: 1});
// dispatcher.emit('click', {foo: 2});
// dispatcher.emit('error', new Error('Alert!'));

const fs = require('fs');

// fs.writeFileSync('test.txt', 'Hello world!'); // новый файл
// let data = fs.readFileSync('test.txt',{encoding: 'utf-8'});
//
// console.log(data);


fs.writeFile('test.txt', 'Hello world!', function(err){
    if (err) throw new Error(err);

    fs.rename('test.txt', 'test2.txt', function(err){
        if (err) throw new Error(err);

        fs.readFile('test2.txt', {encoding: 'utf-8'},  function(err, data){
            if (err) throw new Error(err);
            console.log(data)
        })
    });


});